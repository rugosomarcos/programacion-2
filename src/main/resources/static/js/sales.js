
$(document).ready(function() {
  
  getSales();
  $('#sales');

   	
  
});



async function getGames(){
	
  const request = await fetch('games/list', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    
  });
  const games = await request.json();
  
  
  
  let listGames = '';
  for (let game of games){
  	
  	let gameHTML = '<tr><td>' + game.title +' </td><td>'

	
	listGames += gameHTML;
	}
	
//document.querySelector('#games tbody').outerHTML = listGames; 
document.querySelector('#games li').outerHTML = listGames; 
}



async function deleteGame(id){
	
  if (!confirm('¿Desea eliminar este juego?')){
	
	return;
  }
  const deleteRequest = await fetch('games/delete/' + id, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    
    
  });
  
  location.reload()	

}

async function addSale(){
	
  let data = {};
  
  data.client_id = document.getElementById('clientId').value;
  data.game_id = document.getElementById('gameId').value;
  
  const postRequest = await fetch('sales/save', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    
    body: JSON.stringify(data)
  });
  
  alert("Se realizo la venta");
  
  window.location.href = 'sales.html'	

}


async function getSales(){
	
  const request = await fetch('sales/list', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    
  });
  const sales = await request.json();

  
  let listSales = '';
  for (let sale of sales){

    
  	let saleHTML = '<tr><td>'+ sale.id +'</td><td>'+ sale.client_id +' </td><td>'+ sale.game_id + '</td><td>';
	
	
	listSales += saleHTML;
	}
	
  
document.querySelector('#sales tbody').outerHTML = listSales; 
  
}


