// Call the dataTables jQuery plugin
$(document).ready(function() {
  
  getClients();
  $('#clients').DataTable();
  
});


async function getClients(){
	
  const request = await fetch('clients/list', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    
  });
  const clients = await request.json();

  
  let listClients = '';
  for (let client of clients){

    let deleteButton = '<a href="#" onclick="deleteClient('+ client.id +')" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></a>';
  	let editButton = '<a href="#" onclick="editClient('+ client.id +')" class="btn btn-success btn-circle"><i class="fas fa-edit"></i></a>';
  	let clientHTML = '<tr><td>'+ client.id +'</td><td>'+ client.dni +' </td><td>'+ client.name +' </td><td>'+ client.lastName +'</td><td>'
  	+ client.phone +'</td><td>'+ client.email +
  	'</td><td>' + deleteButton + '</td><td>'+ '</td><td>'+ editButton + '</td></tr>';
	
	
	listClients += clientHTML;
	}
	
  
document.querySelector('#clients tbody').outerHTML = listClients; 
  
}

async function deleteClient(id){
	
  if (!confirm('¿Desea eliminar este cliente?')){
	
	return;
  }
  const deleteRequest = await fetch('clients/delete/' + id, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    
    
  });
  
  location.reload()	

}

async function addClient(){
	
  let data = {};
 
  data.dni = document.getElementById('clientDNI').value; 
  data.name = document.getElementById('clientName').value;
  data.lastName = document.getElementById('clientLastName').value;
  data.phone = document.getElementById('clientPhone').value;
  data.email = document.getElementById('clientEmail').value;
  
  const postRequest = await fetch('clients/save', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    
    body: JSON.stringify(data)
  });
  
  alert("El cliente fue agregado con éxito");
  
  window.location.href = 'clients.html'	

}