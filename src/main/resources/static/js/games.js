// Call the dataTables jQuery plugin
$(document).ready(function() {
  
  getGames();
  $('#games').DataTable();
  
});


async function getGames(){
	
  const request = await fetch('games/list', {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    
  });
  const games = await request.json();
  
  
  
  let listGames = '';
  for (let game of games){
  	
  	let deleteButton = '<a href="#" onclick="deleteGame('+ game.id +')" class="btn btn-danger btn-circle"><i class="fas fa-trash"></i></a>';
  	
  	let gameHTML = '<tr><td>'+ game.id +'</td><td>'+ game.code + '</td><td>'+ game.title +' </td><td>'+ game.type +'</td><td>'
  	+ game.platform +'</td><td>'+ game.price + '</td><td>'
  	+ game.quantity +'</td><td>'+ deleteButton + '</td></tr>';
	
	
	listGames += gameHTML;
	}
	
document.querySelector('#games tbody').outerHTML = listGames; 
}

async function deleteGame(id){
	
  if (!confirm('¿Desea eliminar este juego?')){
	
	return;
  }
  const deleteRequest = await fetch('games/delete/' + id, {
    method: 'DELETE',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    
    
  });
  
  location.reload()	

}

async function addGame(){
	
  let data = {};
  data.code = document.getElementById('gameCode').value;
  data.title = document.getElementById('gameTitle').value;
  data.type = document.getElementById('gameType').value;
  data.platform = document.getElementById('gamePlatform').value;
  data.price = document.getElementById('gamePrice').value;
  data.quantity = document.getElementById('gameQuantity').value;
  
  const postRequest = await fetch('games/save', {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    
    body: JSON.stringify(data)
  });
  
  alert("El cliente fue agregado con éxito");
  
  window.location.href = 'games.html'	

}