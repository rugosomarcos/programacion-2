package com.um.dao;



import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.um.models.Client;


public interface ClientDAO extends JpaRepository<Client, Long> {

	

	List<Client> findByDni(Integer dni);
	
	

	
 
}
 