package com.um.dao;


import org.springframework.data.jpa.repository.JpaRepository;


import com.um.models.Game;

public interface GameDAO extends JpaRepository<Game, Integer>{

	Game findFirstByCode(String code);

	

	
	
}
