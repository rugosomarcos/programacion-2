package com.um.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.um.models.GameSold;

public interface GameSoldDAO extends JpaRepository<GameSold, Integer> {

	

}
