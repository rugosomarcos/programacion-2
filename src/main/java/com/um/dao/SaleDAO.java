package com.um.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.um.models.Sale;

public interface SaleDAO extends JpaRepository<Sale, Integer> {

	
}

