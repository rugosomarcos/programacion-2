package com.um;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Progr2Application {

	public static void main(String[] args) {
		SpringApplication.run(Progr2Application.class, args);
	}

}
