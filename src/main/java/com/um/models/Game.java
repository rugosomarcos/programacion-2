package com.um.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Game {	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column
	private String code;
	
	@Column
	private String title;

	@Column
	private String type;
	
	@Column
	private String platform;
	
	@Column
	private float price;

	@Column
	private float existance;
	
	public Game(String code, String title, String type, String platform, float price, float existance, Integer id) {
        this.code = code;
        this.title = title;
        this.type = type;
        this.platform = platform;
        this.price = price;
        this.existance = existance;
        this.id = id;
    }
	
	
	public Game(String code, String title, String type, String platform, float price, float existance) {
        this.code = code;
        this.title = title;
        this.type = type;
        this.platform = platform;
        this.price = price;
        this.existance = existance;
    }

	
	public Game() {
    }
	
	
	public float getExistance() {
		return existance;
	}

	public void setExistance(float existance) {
		this.existance = existance;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPlatform() {
		return platform;
	}

	public void setPlatform(String platform) {
		this.platform = platform;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean noAvaliable() {
        return this.existance <= 0;
    }

	public void substractExistance(float existance) {
        this.existance -= existance;
    }


	public void setPrice(float price) {
		this.price = price;
	}
}
