package com.um.models;

public class GameForSale extends Game {

	private float quantity;
	
	public GameForSale(String code, String title, String type, String platform, float price, float quantity, Integer id, float existance) {
        super(code, title, type, platform, price, existance, id);
        this.quantity = quantity;
    }
	
	public GameForSale(String code, String title, String type, String platform, float price, float existance, float quantity) {
        super(code, title, type, platform, price, existance);
        this.quantity = quantity;
    }
	
	public void increaseQuantity() {
        this.quantity++;
    }
 
    public float getTotal() {
        return this.getPrice() * this.quantity;
    }

	public float getQuantity() {
		return quantity;
	}




	
}
