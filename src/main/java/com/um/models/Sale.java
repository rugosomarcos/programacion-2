package com.um.models;

import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;




@Entity
public class Sale {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)

	private Integer id;
	@Column
	private String date;
	
	@OneToMany(mappedBy = "sale", cascade = CascadeType.ALL)
    private Set<GameSold> games;
	
	public Sale() {
        this.date = Functions.getDate();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public float getTotal() {
        float total = 0;
        for (GameSold gameSold : this.games) {
            total += gameSold.getTotal();
        }
        return total;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Set<GameSold> getGames() {
        return games;
    }

    public void setGames(Set<GameSold> games) {
        this.games = games;
    }

}
