package com.um.models;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

class Functions {

	static String getDate() {
		
		String format = "yyyy-MM-dd HH:mm:ss";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        LocalDateTime now = LocalDateTime.now();
        return formatter.format(now);
	

		}
}