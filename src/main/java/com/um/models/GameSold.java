package com.um.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class GameSold {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Float quantity, price;
    private String title, code;
    @ManyToOne
    @JoinColumn
    private Sale sale;

    public GameSold(float quantity, float price, String title, String code, Sale sale) {
        this.quantity = quantity;
        this.price = price;
        this.title = title;
        this.code = code;
        this.sale = sale;
    }
    
    public GameSold() {
    }
    
    public Float getTotal() {
        return this.quantity * this.price;
    }
    
    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
		this.sale = sale;
	}
    
	public Float getQuantity() {
		return quantity;
	}


	public void setQuantity(Float quantity) {
		this.quantity = quantity;
	}


	public Float getPrice() {
		return price;
	}


	public void setPrice(Float price) {
		this.price = price;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}




	
}
