package com.um.controllers;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.um.dao.GameDAO;
import com.um.dao.GameSoldDAO;
import com.um.dao.SaleDAO;
import com.um.models.Game;
import com.um.models.GameForSale;
import com.um.models.GameSold;
import com.um.models.Sale;


@Controller
@RequestMapping(path = "/sell")
public class SellRest {

	@Autowired
    private GameDAO gameDAO;
    @Autowired
    private SaleDAO saleDAO;
    @Autowired
    private GameSoldDAO gameSoldDAO;

    @PostMapping(value = "/remove/{index}")
    public String deleteInSalesCart(@PathVariable int index,HttpServletRequest request) {
        ArrayList<GameForSale> salesCart = this.getSalesCart(request);
        if (salesCart != null && salesCart.size() > 0 && salesCart.get(index) != null) {
            salesCart.remove(index);
            this.saveSalesCart(salesCart, request);
        }
        return "redirect:/sell/";
    }
    
    private void cleanSalesCart(HttpServletRequest request) {
        this.saveSalesCart(new ArrayList<>(), request);
    }
    
    @GetMapping(value = "/clean")
    public String cancelSale(HttpServletRequest request, RedirectAttributes redirectAttrs) {
        this.cleanSalesCart(request);
        redirectAttrs
                .addFlashAttribute("mensaje", "Venta cancelada")
                .addFlashAttribute("clase", "info");
        return "redirect:/sell/";
    }
    
	
    @PostMapping(value = "/finish")
    public String finishSale(HttpServletRequest request, RedirectAttributes redirectAttrs) {
        ArrayList<GameForSale> salesCart = this.getSalesCart(request);
        // Si no hay carrito o está vacío, regresamos inmediatamente
        if (salesCart == null || salesCart.size() <= 0) {
            return "redirect:/sell/";
        }
        Sale s = saleDAO.save(new Sale());
        // Recorrer el carrito
        for (GameForSale gameForSale : salesCart) {
            // Obtener el producto fresco desde la base de datos
            Game g = gameDAO.findById(gameForSale.getId()).orElse(null);
            if (g == null) continue; // Si es nulo o no existe, ignoramos el siguiente código con continue
            // Le restamos existencia
            g.substractExistance(gameForSale.getQuantity());
            // Lo guardamos con la existencia ya restada
            gameDAO.save(g);
            // Creamos un nuevo producto que será el que se guarda junto con la venta
            								
            GameSold gameSold = new GameSold(gameForSale.getQuantity(), gameForSale.getPrice(), gameForSale.getTitle(), gameForSale.getCode(), s);
            // Y lo guardamos
            gameSoldDAO.save(gameSold);
        	}

        // Al final limpiamos el carrito
        this.cleanSalesCart(request);
        // e indicamos una venta exitosa
        redirectAttrs
                .addFlashAttribute("mensaje", "Venta realizada correctamente")
                .addFlashAttribute("clase", "success");
        return "redirect:/sell/";
    	}
    
    @GetMapping(value = "/")
    public String SellInterface(Model model, HttpServletRequest request) {
        model.addAttribute("game", new Game());
        float total = 0;
        ArrayList<GameForSale> salesCart = this.getSalesCart(request);
        for (GameForSale g: salesCart) total += g.getTotal();
        model.addAttribute("total", total);
        return "sell/sell";
    }

    private ArrayList<GameForSale> getSalesCart(HttpServletRequest request) {
        @SuppressWarnings("unchecked")
		ArrayList<GameForSale> salesCart = (ArrayList<GameForSale>) request.getSession().getAttribute("salesCart");
        if (salesCart == null) {
            salesCart = new ArrayList<>();
        }
        return salesCart;
    }

    private void saveSalesCart(ArrayList<GameForSale> salesCart, HttpServletRequest request) {
        request.getSession().setAttribute("salesCart", salesCart);
    }

    @PostMapping(value = "/add")
    public String addToSalesCart(@ModelAttribute Game game, HttpServletRequest request, RedirectAttributes redirectAttrs) {
        ArrayList<GameForSale> salesCart = this.getSalesCart(request);
        Game gameFindByCode = gameDAO.findFirstByCode(game.getCode());
        if (gameFindByCode == null) {
            redirectAttrs
                    .addFlashAttribute("mensaje", "El producto con el código " + game.getCode() + " no existe");
                    
            return "redirect:/sell/";
        }
        if (gameFindByCode.noAvaliable()) {
            redirectAttrs
                    .addFlashAttribute("mensaje", "El producto está agotado")
                    .addFlashAttribute("clase", "warning");
            return "redirect:/sell/";
        }
        boolean result = false;
        for (GameForSale gameForSaleActual : salesCart) {
            if (gameForSaleActual.getCode().equals(gameFindByCode.getCode())) {
            	gameForSaleActual.increaseQuantity();
            	result = true;
                break;
            }
        }
        if (!result) {
        								
        	salesCart.add(new GameForSale(gameFindByCode.getCode(), gameFindByCode.getTitle(), gameFindByCode.getType(), gameFindByCode.getPlatform(), gameFindByCode.getPrice(), 1f, gameFindByCode.getId(),1f ));
        }
        this.saveSalesCart(salesCart, request);
        System.out.print(salesCart);
        return "redirect:/sell/";
    }
    
    
    
}
