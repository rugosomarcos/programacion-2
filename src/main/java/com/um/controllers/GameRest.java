package com.um.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.um.dao.GameDAO;
import com.um.models.Game;


@Controller
@RequestMapping(path = "/games")
public class GameRest {

	@Autowired
	private GameDAO gameDAO;

	//POST
	
	@GetMapping(value = "/addGame")
    public String addGame(Model model) {
        model.addAttribute("game", new Game());
        return "games/add_games";
	
	}
	@GetMapping(value = "/listGames")
	public String listGames(Model model) {
        model.addAttribute("games", gameDAO.findAll());
        return "games/list_games";
    }
	
	@PostMapping(value = "/delete")
    public String deleteGame(@ModelAttribute Game game, RedirectAttributes redirectAttrs) {
        redirectAttrs
                .addFlashAttribute("mensaje", "Eliminado correctamente")
                .addFlashAttribute("clase", "warning");
        gameDAO.deleteById(game.getId());
        return "redirect:/listGames";
    }
	
	@PostMapping(value = "/editGame/{id}")
    public String updateGame(@ModelAttribute Game game, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            if (game.getId() != null) {
                return "games/edit_games";
            }
            return "redirect:/games/listGames";
        }
        Game gameValidation = gameDAO.findFirstByCode(game.getCode());

        if (gameValidation != null && !gameValidation.getId().equals(game.getId())) {
            redirectAttrs
                    .addFlashAttribute("mensaje", "Ya existe un producto con ese código")
                    .addFlashAttribute("clase", "warning");
            return "redirect:/games/addGame";
        }
        gameDAO.save(game);
        redirectAttrs
                .addFlashAttribute("mensaje", "Editado correctamente")
                .addFlashAttribute("clase", "success");
        return "redirect:/games/listGames";
    }

    @GetMapping(value = "/editGame/{id}")
    public String updateGame(@PathVariable int id, Model model) {
        model.addAttribute("game", gameDAO.findById(id).orElse(null));
        return "games/edit_games";
    }
	
	@PostMapping(value = "/addGame")
    public String saveGame(@ModelAttribute Game game, BindingResult bindingResult, RedirectAttributes redirectAttrs) {
        if (bindingResult.hasErrors()) {
            return "games/add_game";
        }
        if (gameDAO.findFirstByCode(game.getCode()) != null) {
            redirectAttrs
                    .addFlashAttribute("mensaje", "Ya existe un producto con ese código")
                    .addFlashAttribute("clase", "warning");
            return "redirect:/games/addGame";
        }
        gameDAO.save(game);
        
        redirectAttrs
                .addFlashAttribute("mensaje", "Agregado correctamente")
                .addFlashAttribute("clase", "success");
        return "redirect:/addGame";
    }
}
