package com.um.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.um.dao.SaleDAO;

@Controller
@RequestMapping(path = "/sales")
public class SaleRest {

	@Autowired
	private SaleDAO saleDAO;

		
	//POST
	@GetMapping(value = "/")
    public String listSales(Model model) {
        model.addAttribute("sales", saleDAO.findAll());
        
        return "sales/sales";
    }
	
	
}

