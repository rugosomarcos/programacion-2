package com.um.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.um.dao.ClientDAO;
import com.um.models.Client;


@RestController 
@RequestMapping("clients")
public class ClientRest {

	@Autowired
	private ClientDAO ClientDAO;
	
	
	//POST
	@PostMapping("/save")
	public void save(@RequestBody Client client){
		
	
		ClientDAO.save(client);
		if (ClientDAO.findByDni(client.getDni()).isEmpty())
		{
			ClientDAO.save(client);
			System.out.print("No existe con ese dni, se agrego");
		}
		else {
			
			System.out.print("Existe con ese dni");
		}
		
		 
		 
	}
	
	@GetMapping("/list")
	public List<Client> list(){
		
		
		return ClientDAO.findAll();
	}
	
	
	
	@DeleteMapping("/delete/{id}")
	public void delete(@PathVariable("id") Long id){
		ClientDAO.deleteById(id);
	}
	
	@PutMapping("/update")
	public void update(@RequestBody Client client){
		ClientDAO.save(client);
	}
}
